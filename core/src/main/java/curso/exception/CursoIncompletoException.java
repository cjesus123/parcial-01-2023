package curso.exception;

public class CursoIncompletoException extends RuntimeException {
    public CursoIncompletoException(String s) {
        super(s);
    }
}
