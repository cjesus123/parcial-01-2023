package curso.modelo;

import curso.exception.CursoIncompletoException;
import curso.exception.CursoIncorrectoException;

import java.time.LocalDate;

public class Curso{

    private Integer id;
    private String nombre;
    private LocalDate fecha_cierre_inscripcion;
    private String nivel;

    private Curso(Integer id, String nombre, LocalDate fecha_cierre_inscripcion, String nivel) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_cierre_inscripcion = fecha_cierre_inscripcion;
        this.nivel = nivel;
    }

    public static Curso instancia(Integer id,String nombre,LocalDate fecha_cierre_inscripcion,String nivel){
        if(nombre == null || nombre.isBlank()){
            throw new CursoIncompletoException("El nombre del Curso es obligatorio");
        }
        if(fecha_cierre_inscripcion == null){
            throw new CursoIncompletoException("La fecha de cierre del Curso es obligatoria");
        }
        if(fecha_cierre_inscripcion.isBefore(LocalDate.now())){
            throw new CursoIncorrectoException("La fecha de cierre del Curso no puede ser inferior a la actual");
        }
        if(nivel == null || nivel.isBlank()){
            throw new CursoIncompletoException("El nivel del Curso es obligatorio");
        }
        return new Curso(id,nombre,fecha_cierre_inscripcion,nivel);
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFecha_cierre_inscripcion() {
        return fecha_cierre_inscripcion;
    }

    public String getNivel() {
        return nivel;
    }
}
