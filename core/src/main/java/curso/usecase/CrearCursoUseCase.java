package curso.usecase;

import curso.exception.CursoExisteException;
import curso.input.CrearCursoInput;
import curso.modelo.Curso;
import curso.output.CrearCursoRepositorio;

public class CrearCursoUseCase implements CrearCursoInput {

    CrearCursoRepositorio crearCursoRepositorio;

    public CrearCursoUseCase(CrearCursoRepositorio crearCursoRepositorio) {
        this.crearCursoRepositorio = crearCursoRepositorio;
    }

    @Override
    public Integer crearCurso(Curso nuevoCurso) {
        if(crearCursoRepositorio.existePorNombre(nuevoCurso.getNombre())){
            throw new CursoExisteException("Ya existe un Curso con nombre " + nuevoCurso.getNombre());
        }
        return crearCursoRepositorio.guardar(nuevoCurso);
    }
}
