package ar.edu.undec.adapter.data.curso.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.ConsultarCursoCRUD;
import ar.edu.undec.adapter.data.curso.mapper.CursoDataMapper;
import curso.modelo.Curso;
import curso.output.ConsultarCursoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class ConsultarCursoRepositorioImplementacion implements ConsultarCursoRepositorio{

    ConsultarCursoCRUD consultarCursoCRUD;

    @Autowired
    public ConsultarCursoRepositorioImplementacion(ConsultarCursoCRUD consultarCursoCRUD) {
        this.consultarCursoCRUD = consultarCursoCRUD;
    }

    @Override
    public Collection<Curso> obtenerCurso() {
        return consultarCursoCRUD.findAll().stream().map(CursoDataMapper::EntidadACore).collect(Collectors.toList());
    }
}
