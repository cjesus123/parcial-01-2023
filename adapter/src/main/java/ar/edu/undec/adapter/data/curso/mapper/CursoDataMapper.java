package ar.edu.undec.adapter.data.curso.mapper;

import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import ar.edu.undec.adapter.data.exception.FailedMapperException;
import curso.modelo.Curso;

public class CursoDataMapper{

    public static Curso EntidadACore(CursoEntity cursoEntity){
        try{
            return Curso.instancia(cursoEntity.getId(), cursoEntity.getNombre(), cursoEntity.getFecha_cierre_inscripcion(),cursoEntity.getNivel());
        }catch (RuntimeException runtimeException){
            throw new FailedMapperException("Error al mapear de entidad a Core");
        }
    }

    public static CursoEntity CoreAEntidad(Curso curso){
        try{
            return new CursoEntity(curso.getId(),curso.getNombre(),curso.getFecha_cierre_inscripcion(),curso.getNivel());
        }catch (RuntimeException runtimeException){
            throw new FailedMapperException("Error al mapear de Core a Entidad");
        }
    }
}
