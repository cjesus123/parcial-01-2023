package ar.edu.undec.adapter.service.curso.beanconfig;

import curso.input.ConsultarCursoInput;
import curso.input.CrearCursoInput;
import curso.output.ConsultarCursoRepositorio;
import curso.output.CrearCursoRepositorio;
import curso.usecase.ConsultarCursoUseCase;
import curso.usecase.CrearCursoUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CursoConfig {

    @Bean
    public CrearCursoInput crearCursoInput(CrearCursoRepositorio crearCursoRepositorio){
        return new CrearCursoUseCase(crearCursoRepositorio);
    }

    @Bean
    public ConsultarCursoInput consultarCursoInput(ConsultarCursoRepositorio consultarCursoRepositorio){
        return new ConsultarCursoUseCase(consultarCursoRepositorio);

    }
}
