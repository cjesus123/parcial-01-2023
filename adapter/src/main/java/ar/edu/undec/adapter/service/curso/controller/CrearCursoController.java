package ar.edu.undec.adapter.service.curso.controller;

import ar.edu.undec.adapter.service.curso.mapper.CursoServiceMapper;
import ar.edu.undec.adapter.service.curso.model.CursoDTO;
import curso.input.CrearCursoInput;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@RequestMapping("curso")
public class CrearCursoController{

    CrearCursoInput crearCursoInput;

    @Autowired
    public CrearCursoController(CrearCursoInput crearCursoInput) {
        this.crearCursoInput = crearCursoInput;
    }

    @PostMapping
    public ResponseEntity<?> crearCurso(@RequestBody CursoDTO cursoDTO) {
        try {
            return ResponseEntity.created(URI.create("")).body(crearCursoInput.crearCurso(CursoServiceMapper.EntidadACore(cursoDTO)));
        } catch (Exception runtimeException) {
            return ResponseEntity.badRequest().body(runtimeException.getMessage());
        }
    }
}
