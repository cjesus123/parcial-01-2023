package ar.edu.undec.adapter.service.curso.controller;

import ar.edu.undec.adapter.service.curso.mapper.CursoServiceMapper;
import ar.edu.undec.adapter.service.curso.model.CursoDTO;
import curso.input.ConsultarCursoInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("curso")
public class ConsultarCursosController{

    ConsultarCursoInput consultarCursoInput;

    @Autowired
    public ConsultarCursosController(ConsultarCursoInput consultarCursoInput) {
        this.consultarCursoInput = consultarCursoInput;
    }

    @GetMapping
    public ResponseEntity<List<CursoDTO>> consultarCursos(){
        if(consultarCursoInput.consultarCurso().isEmpty()){
            return ResponseEntity.noContent().build();
        }
        try{
            return ResponseEntity.ok().body(consultarCursoInput.consultarCurso().stream().map(CursoServiceMapper::CoreAEntidad).collect(Collectors.toList()));
        }catch (RuntimeException runtimeException){
            return ResponseEntity.noContent().build();
        }
    }
}
