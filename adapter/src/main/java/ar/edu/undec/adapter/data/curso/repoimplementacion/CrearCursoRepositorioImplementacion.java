package ar.edu.undec.adapter.data.curso.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.CrearCursoCrud;
import ar.edu.undec.adapter.data.curso.mapper.CursoDataMapper;
import ar.edu.undec.adapter.data.exception.DataBaseException;
import curso.modelo.Curso;
import curso.output.CrearCursoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrearCursoRepositorioImplementacion implements CrearCursoRepositorio{

    CrearCursoCrud crearCursoCrud;

    @Autowired
    public CrearCursoRepositorioImplementacion(CrearCursoCrud crearCursoCrud) {
        this.crearCursoCrud = crearCursoCrud;
    }

    @Override
    public Boolean existePorNombre(String nombre) {
        return crearCursoCrud.existsByNombre(nombre);
    }

    @Override
    public Integer guardar(Curso curso) {
        try {
            return crearCursoCrud.save(CursoDataMapper.CoreAEntidad(curso)).getId();
        }catch (RuntimeException runtimeException){
            throw new DataBaseException("Error al guardar en la base de datos");
        }
    }
}
