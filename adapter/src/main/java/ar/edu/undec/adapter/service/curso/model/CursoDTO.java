package ar.edu.undec.adapter.service.curso.model;

import java.time.LocalDate;

public class CursoDTO{
    private Integer id;
    private String nombre;
    private LocalDate fecha_cierre_inscripcion;
    private String nivel;

    public CursoDTO(){}

    public CursoDTO(Integer id, String nombre, LocalDate fecha_cierre_inscripcion, String nivel) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_cierre_inscripcion = fecha_cierre_inscripcion;
        this.nivel = nivel;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFecha_cierre_inscripcion() {
        return fecha_cierre_inscripcion;
    }

    public String getNivel() {
        return nivel;
    }
}
