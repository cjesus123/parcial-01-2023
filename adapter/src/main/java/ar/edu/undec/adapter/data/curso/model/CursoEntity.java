package ar.edu.undec.adapter.data.curso.model;


import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "curso")
public class CursoEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String nombre;
    private LocalDate fecha_cierre_inscripcion;

    private String nivel;

    public CursoEntity(Integer id, String nombre, LocalDate fecha_cierre_inscripcion, String nivel) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_cierre_inscripcion = fecha_cierre_inscripcion;
        this.nivel = nivel;
    }

    public CursoEntity() {
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFecha_cierre_inscripcion() {
        return fecha_cierre_inscripcion;
    }

    public String getNivel() {
        return nivel;
    }
}
