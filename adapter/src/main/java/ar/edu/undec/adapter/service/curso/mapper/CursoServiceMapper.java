package ar.edu.undec.adapter.service.curso.mapper;

import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import ar.edu.undec.adapter.data.exception.FailedMapperException;
import ar.edu.undec.adapter.service.curso.model.CursoDTO;
import curso.modelo.Curso;

public class CursoServiceMapper{

    public static Curso EntidadACore(CursoDTO cursoDTO){
        try{
            return Curso.instancia(cursoDTO.getId(), cursoDTO.getNombre(), cursoDTO.getFecha_cierre_inscripcion(),cursoDTO.getNivel());
        }catch (RuntimeException runtimeException){
            throw new FailedMapperException("Error al mapear DTO a Core");
        }
    }

    public static CursoDTO CoreAEntidad(Curso curso){
        try{
            return new CursoDTO(curso.getId(),curso.getNombre(),curso.getFecha_cierre_inscripcion(),curso.getNivel());
        }catch (RuntimeException runtimeException){
            throw new FailedMapperException("Error al mapear core a DTO");
        }
    }
}
